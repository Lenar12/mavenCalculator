package pl.ncdc.calculator;

import org.junit.Assert;
import org.junit.Test;


public class CalculatorTest {
	private static CalculatorController calculator = new CalculatorController();
	@Test
	public void calculatorOperationAddSuccess() {
	    float first = (float) 5.10;
		float second = (float) 0.2;
		
		Assert.assertTrue("Test dodawania wystąpił błąd", first+second==calculator.add(first, second));
	}
	@Test
	public void calculatorOperationsSubstractSuccess(){
		float first = (float) 5.10;
		float second = (float) 0.2;
		
		Assert.assertTrue("Test odejmowania wystąpił błąd", first-second==calculator.substract(first, second));
		Assert.assertEquals(4.9, calculator.substract(first, second),0.01);
	}
	
	@Test
	public void calculatorOperationsDivideSuccess(){
		float first = (float) 2.0;
		float second = (float) 2.0;
		
		Assert.assertTrue("Test dzielenia wystąpił błąd", first/second==calculator.divide(first, second));
		Assert.assertEquals(first/second, calculator.divide(first, second),0);
		Assert.assertEquals(0, calculator.divide(first, 0),0);
	}
	@Test
	public void calculatorOperationsMultiplySuccess(){
		float first = (float) 2.0;
		float second = (float) 2.0;
		
		Assert.assertTrue("Test mnozenia wystąpił błąd", first*second==calculator.multiply(first, second));
		Assert.assertEquals(4, calculator.multiply(first, second),0.01);
	}
	@Test
	public void calculatorControllerTest(){
		float first = (float) 2.0;
		float second = (float) 2.0;
		
		Assert.assertTrue("Controller multiply", first/second==calculator.calculate(first, second,"/"));
		Assert.assertTrue("Controller substract", first-second==calculator.calculate(first, second,"-"));
		Assert.assertTrue("Controller add", first+second==calculator.calculate(first, second,"+"));
		Assert.assertEquals(4, calculator.calculate(first, second,"*"),0.01);
	}

}

