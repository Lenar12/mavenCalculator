package pl.ncdc.calculator;


import java.util.Scanner;

public class Main {

	public static void main(String[] args) {
		runnableAplication();	
	}
	
	private static void runnableAplication(){
		CalculatorController calculator = new CalculatorController();
		@SuppressWarnings("resource")
		Scanner scanner = new Scanner(System.in);
		
		System.out.println("Podaj pierwszą liczbę: ");
		int firstNumber = scanner.nextInt();
		System.out.println("Podaj drugą liczbę: ");
		int secondNumber = scanner.nextInt();
		System.out.println("Podaj działanie: ");
		String symbol = scanner.next();
		
		System.out.print("Wynik działania = "+ calculator.calculate(firstNumber, secondNumber, symbol));
	}

}
