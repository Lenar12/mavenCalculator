package pl.ncdc.calculator;


public class Calculator {
	public Calculator(){};
	
	public float add(float first, float second){
		return first+second;
	}
	
	public float substract(float first, float second){
		return first-second;
	}
	
	public float divide(float first, float second){
		if(second!=0)
			return first/second;
		
		return 0;
	}
	
	public float multiply(float first, float second){
		return first*second;
	}
	
}
